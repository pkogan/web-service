<?php
/* @var $this WebserviceController */

$this->breadcrumbs=array(
	'Webservice',
);
?>
<h1>Cliente de web service</h1>
<h2>Mensaje Inicial wsdl -> y <-</h2>

<a href="<?php echo $wsdl;?>"  ><?php echo htmlentities($wsdl);?> </a>

<hr>
<h3>Funciones</h3>
<pre>
<?php print_r($funciones);?>
</pre>
<h3>Tipos</h3>
<pre>
<?php print_r($tipos);?>
</pre>

<hr>
<h2>Mensaje Petición soap  -></h2>
<hr>
<?php echo CHtml::link('ver xml',array($this->action->id,'tipo'=>'pedido')) ?>
<pre class="code">
<?php echo str_replace('&gt;&lt;','&gt;<br/>&lt;',htmlentities($pedido));?>
</pre>
<hr>
<h2>Mensaje Respuesta soap <-</h2>
<hr>
<?php echo CHtml::link('ver xml',array($this->action->id,'tipo'=>'respuesta')) ?>
<pre class="code">
<?php echo str_replace('&gt;&lt;','&gt;<br/>&lt;',htmlentities($respuesta));?>
</pre>
<hr>
<h2>Dato:</h2>

<pre>
<?php print_r($dato); ?>
</pre>
