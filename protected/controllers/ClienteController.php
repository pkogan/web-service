<?php

class ClienteController extends Controller {

    public function actionIndex() {
        /*
         * Ver Parámetros del objeto SoapClient
         */
        $wsdl = 'http://localhost/yii/ws/index.php?r=webservice/wsdl';
        $client = new SoapClient($wsdl, array('trace' => TRUE, 'cache_wsdl' => WSDL_CACHE_NONE));

        $funciones = $client->__getFunctions();
        $tipos = $client->__getTypes();

        $dato = 'getPrice("IBM")=' . $client->getPrice("IBM");
        //$dato = $client->suma(8,29);
        //$dato = $client->listastring();
        //$dato = $client->horaservidor();
        //$dato = $client->arrayregistros();
        
      

        $pedido = $client->__getLastRequest();

        $respuesta = $client->__getLastResponse();


        if (isset($_GET['tipo'])) {
            header("Content-type: text/xml; charset=utf-8");
            if ($_GET['tipo'] == 'pedido') {
                echo $pedido;
            } else {
                echo $respuesta;
            }
            exit;
        }

        /* $dato.='<br/>suma(8,5)='.$client->suma(5,8);
          $dato.='<br/>listastring()='.implode(',', $client->listastring());

          /*$client = new SoapClient('http://10.0.1.188/yii/reservasRec2/index.php?r=WebService/wsdl');
          $dato = $client->getReserva('56'); */
        $this->render('index', array('dato' => $dato, 'wsdl' => $wsdl, 'funciones' => $funciones, 'tipos' => $tipos, 'pedido' => $pedido, 'respuesta' => $respuesta));
    }


}