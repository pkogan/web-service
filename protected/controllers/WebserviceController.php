<?php
class WebserviceController extends Controller {

    public function actions() {
        return array(
            'wsdl' => array(
                'class' => 'CWebServiceAction',
            ),
        );
    }
    /**
     * da el precio de las acciones de empresa seleccionada
     * @param string the symbol of the stock
     * @return float the stock price
     * @soap
     */
    public function getPrice($symbol) {
        $prices = array('IBM' => 100, 'GOOGLE' => 350,'Moodle'=>1257);
        return isset($prices[$symbol]) ? $prices[$symbol] : 0;
        //...return stock price for $symbol
    }
    /**
     * suma dos numeros
     * 
     * @param int $x Description
     * @param int $y
     * @return int x+y
     * @soap
     */
    public function suma($x,$y){
        return $x+$y;
    }
    /**
     * da una lista de string
     * 
     * @return string[] Description
     * @soap
     */
    public function listastring(){
        return array('uno','dos','tres');
    }
    
    /**
     * Retorna la hora de china
     * @return datetime Hora de china
     * @soap
     */
    public function horaservidor(){
        $date=new DateTime();
        return $date->format('Y-m-d H:i:s');
    }
    
    /**
     * Retorna un array de registros
     * @return Blog[] array de registros
     * @soap
     */
    public function arrayregistros(){
        $registros=  Blog::model()->findAll();
        return $registros;
    }
}